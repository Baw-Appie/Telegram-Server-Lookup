const axios = require('axios')
const crypto = require('crypto')
const fs = require('fs')

function longToIPv4(long) {
  const octets = []
  for (let i = 3; i >= 0; i--) {
    octets.push((long >> (i * 8)) & 0xFF)
  }
  return octets.join('.')
}

async function decryptResponse(body) {
  const raw = Buffer.from(body, 'base64')
  const publicKey = fs.readFileSync('publicKey.pem')
  const rsaPlaintext = crypto.publicDecrypt({ key: publicKey, padding: crypto.constants.RSA_NO_PADDING }, raw)
  const aesCiphertext = rsaPlaintext.slice(32, 32 + 224)

  const decipher = crypto.createDecipheriv('aes-256-cbc', rsaPlaintext.slice(0, 32), rsaPlaintext.slice(16, 32))
  decipher.setAutoPadding(false)
  let rawConfig = decipher.update(aesCiphertext)
  rawConfig = Buffer.concat([rawConfig, decipher.final()])

  const sha256 = crypto.createHash('sha256')
  sha256.update(rawConfig.slice(0, 208))
  const hashDigest = sha256.digest()

  const match = Buffer.compare(hashDigest.slice(0, 16), rawConfig.slice(208, 224)) === 0
  if (!match) {
    console.log('SHA-2 mismatch')
    return
  }
  console.log('SHA-2 digest is correct')

  const data = []
  for (let i = 4; i < rawConfig.length; i += 4) {
    data.push(rawConfig.readUInt32LE(i))
  }
  return data
}

async function decodeV1(data) {
  if(data[0] !== 0xd997c3c5) {
    console.log('Not a V1 message')
    return
  }
  const vectorIndex = data.indexOf(0x1cb5c415)
  const length = data[vectorIndex + 1]

  console.log(`Found a vector of ${length} IP addresses`)
  const tuples = data.slice(vectorIndex + 2, vectorIndex + 2 + length * 2)
  for (let i = 0; i < tuples.length; i += 2) {
    const ipAddress = longToIPv4(tuples[i])
    const port = tuples[i + 1]
    console.log(` * ${ipAddress}:${port}`)
  }
}

async function decodeV3(data) {
  if(data[0] !== 0x5a592a6c) {
    console.log('Not a V3 message')
    return
  }
  console.log(`Timestamp: ${data[1]}`)
  console.log(`Expiration Date: ${data[2]}`)

  const totalCount = data[3]
  console.log(`Total Count: ${totalCount}`)
  let vectorIndex = 4
  
  for(let _ in Array(totalCount).fill()) {
    if(data[vectorIndex] !== 0x4679b65f) {
      console.log('Not a V3 DataCenter message')
      return
    }
    const phonePrefixRules = data[vectorIndex + 1]
    const datacenterId = data[vectorIndex + 2]
    const ipCount = data[vectorIndex + 3]
    console.log(`Phone Prefix Rules: ${phonePrefixRules}`)
    console.log(`Datacenter ID: ${datacenterId}`)
    console.log(`IP Count: ${ipCount}`)
    vectorIndex += 4

    for(let _ in Array(ipCount).fill()) {
      let ipAddress, port, secret
      if(data[vectorIndex] === 0xd433ad73) {
        ipAddress = longToIPv4(data[vectorIndex + 1])
        port = data[vectorIndex + 2]
        vectorIndex += 3
      } else if(data[vectorIndex] === 0x37982646) {
        ipAddress = longToIPv4(data[vectorIndex + 1])
        port = data[vectorIndex + 2]
        secret = data[vectorIndex + 3]
        vectorIndex += 4
      } else {
        console.log('Not a V3 DataCenter IP message')
        continue
      }

      console.log(` * ${ipAddress}:${port} // Secret: ${secret}`)

    }

  }
}

async function download(uri, host) {
  const headers = {}
  if(host) headers['Host'] = host

  try {
    const response = await axios.get(uri, { headers })
    return response.data
  } catch (error) {
    console.log(error)
  }
}

(async () => {
  // TODO: visit https://github.com/TelegramMessenger/Telegram-iOS/blob/release-6.1.2/submodules/MtProtoKit/Sources/MTEncryption.m#L848 for v3

  // https://firestore.googleapis.com/v1/projects/reserve-5a846/databases/(default)/documents/ipconfig for v3


  console.log('****** Google DNS (v1) ******')
  const responseDNSv1 = await download('https://dns.google.com/resolve?name=ap.stel.com&type=16')
  const answersDNSv1 = responseDNSv1.Answer.map((a) => a.data).sort((a, b) => b.length - a.length)
  await decodeV1(await decryptResponse(answersDNSv1.join('')))

  console.log('****** Google DNS (v3) ******')
  const responseDNSv3 = await download('https://dns.google.com/resolve?name=apv3.stel.com&type=16')
  const answersDNSv3 = responseDNSv3.Answer.map((a) => a.data).sort((a, b) => b.length - a.length)
  await decodeV3(await decryptResponse(answersDNSv3.join('')))

  console.log('****** Firebase Realtime (v3) ******')
  const responseFirebaseRealtime = await download('https://reserve-5a846.firebaseio.com/ipconfigv3.json')
  await decodeV3(await decryptResponse(responseFirebaseRealtime))

  console.log('****** Firebase Remote Config (v1) ******')
  const responseFirebaseRemoteConfig = await axios.post(
  'https://firebaseremoteconfig.googleapis.com/v1/projects/peak-vista-421/namespaces/firebase:fetch?key=AIzaSyC2-kAkpDsroixRXw-sTw-Wfqo4NxjMwwM', {
    'app_id': '1:560508485281:web:4ee13a6af4e84d49e67ae0',
    'app_instance_id': '0LvePEyCDfJSK508pOk4aL'  // Should be random
  })
  await decodeV1(await decryptResponse(responseFirebaseRemoteConfig.data.entries.ipconfig))
console.log('****** Firebase Remote Config (v3) ******')
  await decodeV3(await decryptResponse(responseFirebaseRemoteConfig.data.entries.ipconfigv3))

  console.log('****** [Domain Fronted] Firebase Firestore (v3) ******')
  const responseFirebaseFirestore = await download('https://www.google.com/v1/projects/reserve-5a846/databases/(default)/documents/ipconfig', "firestore.googleapis.com")
  await decodeV3(await decryptResponse(responseFirebaseFirestore.documents[0]['fields']['data']['stringValue']))

  console.log('****** Appspot (v1) ******')
  const appspotUri = 'https://dns-telegram.appspot.com'
  const appspotResponse = await download(appspotUri, 'dns-telegram.appspot.com')
  await decodeV1(await decryptResponse(appspotResponse))

  // 403 Forbidden: implemented in TDLib but not on the server side ?
  // console.log('****** Microsoft ******')
  // const microsoftUri = 'https://software-download.microsoft.com/prod/config.txt'
  // const microsoftResponse = await download(microsoftUri, 'tcdnb.azureedge.net')
  // console.log(microsoftResponse)
  // await decodeV1(await decryptResponse(microsoftResponse))
})()
